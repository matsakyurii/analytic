#!/usr/bin/env bash
TIMEOUT_SEC=8
REQUEST_CONCURRENCY=5
REQUEST_COUNT=25

echo `pwd`

docker run --rm --net=host --read-only -v `pwd`:`pwd` -w `pwd` --entrypoint=/bin/sh jordi/ab  \
"-c" \
"if timeout -t ${TIMEOUT_SEC} ab -n ${REQUEST_COUNT} -c ${REQUEST_CONCURRENCY} -T application/json -p post.json -v 2 http://localhost:8099/log; then echo \"Test successful, your code now as 🚀🚀🚀\"; else echo \"Too slow 🐌🐌🐌. Test Failed\"; fi"