<?php

return [

    'use' => 'default',

    'properties' => [

        'default' => [
            'host'                => env('AMQP_HOST','localhost'),
            'port'                => env('AMQP_PORT','5672'),
            'username'            => env('AMQP_USER','rabbitmq'),
            'password'            => env('AMQP_PASSWORD','rabbitmq'),
            'vhost'               => env('AMQP_VHOST', '/'),
            'exchange'            => env('AMQP_EXCHANGE','amq.topic'),
            'exchange_type'       => 'topic',
            'consumer_tag'        => 'consumer',
            'ssl_options'         => [], // See https://secure.php.net/manual/en/context.ssl.php
            'connect_options'     => [], // See https://github.com/php-amqplib/php-amqplib/blob/master/PhpAmqpLib/Connection/AMQPSSLConnection.php
            'queue_properties'    => ['x-ha-policy' => ['S', 'all']],
            'exchange_properties' => [],
            'timeout'             => 0
        ],

    ],

];