<?php

return [
    'storage_path' => storage_path('app') . "/messages.db",
    'buffer_size' => env('BUFFER_SIZE', 10),
    'fast_queue' => env('FAST_QUEUE', 'messages'),
    'slow_queue' => env('SLOW_QUEUE', 'aggregated_messages')
];
