<?php

use App\Application\MessageAggregator;
use App\Domain\Message\Message;

class MessageAggregatorTest extends TestCase
{
    public function testIsCompleteShouldReturnTrue()
    {
        $aggragator = new MessageAggregator(2);
        $message = Message::fromPayload([
            'id_user' => 1,
            'source_label' => "auth22",
            'created_at' => "2019-20-01 12:12:12"
        ]);

        $aggragator->addMessage($message);
        $aggragator->addMessage($message);

        $this->assertTrue($aggragator->isComplete());
    }

    public function testFlushBufferShouldReturnCollectionAndClearBuffer()
    {
        $aggragator = new MessageAggregator(2);
        $message = Message::fromPayload([
            'id_user' => 1,
            'source_label' => "auth22",
            'created_at' => "2019-20-01 12:12:12"
        ]);

        $aggragator->addMessage($message);
        $aggragator->addMessage($message);
        $collection = $aggragator->flushBuffer();

        $this->assertFalse($aggragator->isComplete());
        $this->assertEquals(2, $collection->size());
    }
}