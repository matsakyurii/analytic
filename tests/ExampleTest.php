<?php

use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;

class ExampleTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testExample()
    {
        $aggregator = new \App\Aggregator\MessageAggreagator();
        $aggregator->addMessage(new \App\Aggregator\Message\AnalyticLogMessageInterface(1,'test'));
        $aggregator->addMessage(new \App\Aggregator\Message\AnalyticLogMessageInterface(2,'test2'));
        dd($aggregator->getResultMessage()->toString());
    }
}
