<?php

use App\Domain\Message\Message;

class MessageTest extends TestCase
{
    /**
     *
     */
    public function testHydration()
    {
        $payload = [
            'id_user' => 1,
            'source_label' => "auth22",
            'created_at' => "2019-20-01 12:12:12"
        ];

        $message = Message::fromPayload($payload);
        $this->assertSame($payload["id_user"], $message->getUserId());
        $this->assertSame($payload["source_label"], $message->getSource());
        $this->assertSame($payload["created_at"], $message->createdAt());
    }
}