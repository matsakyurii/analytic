FROM alpine:3.10

# Install packages
RUN apk --no-cache add php7 php7-fpm php7-mysqli php7-json php7-openssl php7-curl \
    php7-zlib php7-xml php7-phar php7-intl php7-dom php7-xmlreader php7-ctype php7-session \
    php7-mbstring php7-gd nginx bash supervisor curl

# Configure nginx
COPY config/nginx/vhost.conf /etc/nginx/nginx.conf
# Remove default server definition
RUN rm /etc/nginx/conf.d/default.conf

# Configure PHP-FPM
COPY config/fpm/fpm-pool.conf /etc/php7/php-fpm.d/www.conf

# Configure supervisord
COPY config/supervisor/supervisord.conf /etc/supervisor/conf.d/supervisord.conf
COPY config/supervisor/* /etc/supervisor/conf.d/

COPY docker/start.sh /usr/local/bin/start

# Make sure files/folders needed by the processes are accessable when they run under the nobody user
RUN chown -R nobody.nobody /run && \
  chown -R nobody.nobody /var/lib/nginx && \
  chown -R nobody.nobody /var/tmp/nginx && \
  chown -R nobody.nobody /var/log/nginx && \
  chown -R nobody.nobody /etc/supervisor/conf.d/ && \
  chown -R nobody.nobody /usr/local/bin/start

# Setup document root
RUN mkdir -p /var/www/html

RUN chmod u+x /usr/local/bin/start

# Switch to use a non-root user from here on
USER nobody

COPY . /var/www/html

# Add application
WORKDIR /var/www/html

# Expose the port nginx is reachable on
EXPOSE 8080

CMD ["/usr/local/bin/start"]
