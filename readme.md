#### how to start:

* install docker( mac, windows ) with docker-compose
* make sure that `8099` and `15672` port in 127.0.0.1 interface is free
* init .env `cp .env.example .env`
* in the project dir run the next commands:
`docker-compose up -d`


#### Install vendors
 `composer install`

#### Usage

send `POST http://localhost:8099/log` with payload
 
`{
 	"id_user": 1,
 	"source_label": "auth",
 	"created_at": "2019-20-01 12:12:12"
 }`
 
#### Benchmark

`cd benchmark`
`bash test.sh`

#### Source code

См. директорию `app` (по умолчанию в фреймворке `Lumen`). Код разделен на слои, характерные для предметной области сервиса. 
`Domain` - сущности и бизнес-логика.
`Application` - клиент/оркестратор доменного слоя.
`Infrastructure` - инфраструктурные имплементации.

Я не нашел нормального пакета для `Lumen` для работы с `AMQP`, пришлось использовать то, что есть.

#### Буфферизация
Запись в store медленная, поэтому запись необходимо делать асинхронно.
Поскольку трафик сообщений очень интенсивный, записывать одно сообщение за одну операцию очень неэффективно.
Я буду применять подход похожий на [Алгоритм Нейгла](https://ru.wikipedia.org/wiki/%D0%90%D0%BB%D0%B3%D0%BE%D1%80%D0%B8%D1%82%D0%BC_%D0%9D%D0%B5%D0%B9%D0%B3%D0%BB%D0%B0 "Алгоритм Нейгла") для системы обмена сообщениями, настроив некоторую буфферизацию сообщений.
Для реализации я применю паттерн `Event Aggregator` (см. `Enterprise Integration Patterns`). 
Условием полноты аггрегатора будет достижение заданного количетсва сообщений в буффере.

#### Multiple ack
Поскольку я считываю множество сообщений в буфер на стороне консьюмера, есть риск их потери при сбое консьюмера.
Поэтому я буду делать multiple ack: я буду забирать последнее добавленное сообщение в буфер и брать его deliveryTag, после чего делать ack(deliveryTag, multiple=true)
Все сообщения с меньшим deliveryTag за последний будут автоматически ack-нуты.

#### Контейнеры
Сервис Аналитики делится на воркер и веб часть.
Для разделения на разные контейнеры я выбрал подход с Container role (app или worker), что будет обработано в скрипте `docker/start.sh`

- `worker` - в контейнере не будет запущен `nginx` и `php-fpm`
- `app` - не будет запущен воркер
