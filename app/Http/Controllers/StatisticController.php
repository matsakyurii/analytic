<?php

namespace App\Http\Controllers;

use Bschmitt\Amqp\Amqp;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Laravel\Lumen\Routing\Controller as BaseController;

class StatisticController extends BaseController
{
    /**
     * @param Request $request
     * @return Response
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    public function __invoke(
        Request $request,
        Amqp $amqp
    ) {
        $this->validate($request, [
            'id_user' => 'required|int',
            'source_label' => 'required|string',
            'created_at' => 'required|string'
        ]);

        $message = json_encode([
            'id_user' => $request->input('id_user'),
            'source_label' => $request->input('source_label'),
            'created_at' => $request->input('created_at')
        ]);

        $amqp->publish(config('app.fast_queue'), $message, ['queue' => config('app.fast_queue')]);

        return \response('Log is tracked', Response::HTTP_ACCEPTED);
    }
}
