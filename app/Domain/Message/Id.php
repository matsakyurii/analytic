<?php

namespace App\Domain\Message;

use Faker\Provider\Uuid;

class Id
{
    /**
     * @var string
     */
    private $id;

    /**
     * @param string $id
     */
    private function __construct(string $id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @return self
     */
    public static function next(): self
    {
        return new self(Uuid::uuid());
    }
}
