<?php

namespace App\Domain\Message;

use Illuminate\Contracts\Support\Arrayable;
use Webmozart\Assert\Assert;

class Message implements Arrayable
{
    /**
     * @var string
     */
    private $id;

    /**
     * @var int
     */
    private $userId;

    /**
     * @var string
     */
    private $source;

    /**
     * @var string $createdAt
     */
    private $createdAt;

    /**
     * @param Id $id
     * @param int $userId
     * @param string $source
     * @param string $date
     */
    public function __construct(Id $id, int $userId, string $source, string $date)
    {
        $this->id = $id->getId();
        $this->userId = $userId;
        $this->source = $source;
        $this->createdAt = $date;
    }

    /**
     * @param array $payload
     *
     * @return Message
     */
    public static function fromPayload(array $payload): self
    {
        Assert::keyExists($payload, 'id_user');
        Assert::keyExists($payload, 'source_label');
        Assert::keyExists($payload, 'created_at');

        return new self(
            Id::next(),
            $payload['id_user'],
            $payload['source_label'],
            $payload['created_at']
        );
    }

    /**
     * @return int
     */
    public function getUserId(): int
    {
        return $this->userId;
    }

    /**
     * @return string
     */
    public function getSource(): string
    {
        return $this->source;
    }

    /**
     * @return string
     */
    public function createdAt(): string
    {
        return $this->createdAt;
    }

    /**
     * @return string
     */
    public function id(): string
    {
        return $this->id;
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return [
            'id' => $this->id,
            'user_id' => $this->userId,
            'source' => $this->source,
            'created_at' => $this->createdAt
        ];
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return (string)json_encode($this->toArray());
    }
}
