<?php

namespace App\Domain\Message;

interface MessageRepository
{
    /**
     * @param Message $message
     *
     * @return mixed
     */
    public function store(Message $message);

    /**
     * @param Collection $messages
     *
     * @return mixed
     */
    public function bulkStore(Collection $messages);
}
