<?php

namespace App\Domain\Message;

class Collection
{
    /**
     * @var Message[]
     */
    private $messages = [];

    /**
     * @param Message $message
     */
    public function addMessage(Message $message)
    {
        $this->messages[] = $message;
    }

    /**
     * @return \Generator
     */
    public function getMessages(): \Generator
    {
        yield from $this->messages;
    }

    /**
     * @return int
     */
    public function size(): int
    {
        return count($this->messages);
    }

    /**
     * @return string
     */
    public function toString(): string
    {
        return implode("\n", $this->messages);
    }
}
