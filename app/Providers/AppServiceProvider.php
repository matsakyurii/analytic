<?php

namespace App\Providers;

use App\Application\MessageAggregator;
use App\Infrastructure\MultipleAckConsumer;
use App\Domain\Message\MessageRepository;
use App\Infrastructure\Message\SlowStorageMessageRepository;
use Illuminate\Support\ServiceProvider;
use Laravel\Lumen\Application;
use SocialTech\SlowStorage;
use SocialTech\StorageInterface;

class AppServiceProvider extends ServiceProvider
{

    public function register()
    {
        //$this->app->configure('database');
       // $this->app->configure('queue');
        $this->app->configure('amqp');
        $this->app->configure('app');

        //$this->app->make('queue');
        $this->app->singleton(StorageInterface::class, SlowStorage::class);
        $this->app->singleton(MessageRepository::class, function (Application $app) {
            return new SlowStorageMessageRepository(
                new SlowStorage(),
                config('app.storage_path')
            );
        });

        $this->app->singleton(MessageAggregator::class, function () {
            return new MessageAggregator(
                config('app.buffer_size')
            );
        });

        $this->app->bind('Bschmitt\Amqp\Consumer', function ($app) {
            return new MultipleAckConsumer($app->config);
        });
    }
}
