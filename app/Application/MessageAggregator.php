<?php

namespace App\Application;

use App\Domain\Message\Collection;
use App\Domain\Message\Message;

class MessageAggregator
{
    /**
     * @var Collection
     */
    private $buffer;

    /**
     * @var int
     */
    private $bufferSize;

    /**
     * @param int $completeBufferSize
     */
    public function __construct(int $completeBufferSize)
    {
        $this->bufferSize = $completeBufferSize;
        $this->buffer = new Collection();
    }

    /**
     * @param Message $message
     */
    public function addMessage(Message $message): void
    {
        $this->buffer->addMessage($message);
    }

    /**
     * @return Collection
     */
    public function flushBuffer(): Collection
    {
        $buffer = $this->buffer;

        $this->buffer = new Collection();

        return $buffer;
    }

    /**
     * @return bool
     */
    public function isComplete(): bool
    {
        return $this->buffer->size() >= $this->bufferSize;
    }
}
