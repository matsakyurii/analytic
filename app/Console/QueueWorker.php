<?php

namespace App\Console;

use App\Domain\Message\Message;
use App\Application\MessageAggregator;
use App\Domain\Message\MessageRepository;
use Bschmitt\Amqp\Amqp;
use Bschmitt\Amqp\Consumer;
use Bschmitt\Amqp\Exception\Configuration;
use Illuminate\Console\Command;

class QueueWorker extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'amqp:consume';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Listen to a queue';

    /**
     * @var Amqp
     */
    private $amqp;

    /**
     * @var MessageRepository $messageRepository
     */
    private $messageRepository;

    /**
     * @var MessageAggregator
     */
    private $aggregator;

    /**
     * @param Amqp $amqp
     * @param MessageRepository $logMessageRepository
     * @param MessageAggregator $messageAggregator
     */
    public function __construct(
        Amqp $amqp,
        MessageRepository $logMessageRepository,
        MessageAggregator $messageAggregator
    ) {
        parent::__construct();

        $this->amqp = $amqp;
        $this->messageRepository = $logMessageRepository;
        $this->aggregator = $messageAggregator;
    }

    /**
     * @throws Configuration
     */
    public function handle()
    {
        $this->amqp->consume(
            config('app.fast_queue'),
            function ($amqpMessage, Consumer $resolver) {
                $payload = json_decode($amqpMessage->body, true);
                $message = Message::fromPayload($payload);
                $this->aggregator->addMessage($message);

                if ($this->aggregator->isComplete()) {
                    $collection = $this->aggregator->flushBuffer();

                    //@todo make custom implementation with multiple acknowledge
                    $resolver->acknowledge($amqpMessage);
                    $this->messageRepository->bulkStore($collection);
                }
            }
        );
    }
}
