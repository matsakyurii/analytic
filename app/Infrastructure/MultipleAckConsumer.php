<?php

namespace App\Infrastructure;

use Bschmitt\Amqp\Consumer;
use PhpAmqpLib\Message\AMQPMessage;

class MultipleAckConsumer extends Consumer
{
    public function acknowledge(AMQPMessage $message)
    {
        $message->delivery_info['channel']->basic_ack($message->delivery_info['delivery_tag'], true);

        if ($message->body === 'quit') {
            $message->delivery_info['channel']->basic_cancel($message->delivery_info['consumer_tag']);
        }
    }
}
