<?php

namespace App\Infrastructure\Message;

use App\Domain\Message\Collection;
use App\Domain\Message\Message;
use App\Domain\Message\MessageRepository;
use SocialTech\StorageInterface;

class SlowStorageMessageRepository implements MessageRepository
{
    /**
     * @var string
     */
    private $filePath;

    /**
     * @var StorageInterface
     */
    private $storage;

    /**
     * @param StorageInterface $storage
     * @param string $filePath
     */
    public function __construct(
        StorageInterface $storage,
        string $filePath
    ) {
        $this->storage = $storage;
        $this->filePath = $filePath;
    }

    /**
     * @param Message $message
     */
    public function store(Message $message)
    {
        $this->storage->append($this->filePath, (string)$message);
    }

    /**
     * @param Collection $messages
     */
    public function bulkStore(Collection $messages): void
    {
        $this->storage->store(
            $this->filePath,
            $messages->toString()
        );
    }
}
