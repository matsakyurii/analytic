#!/usr/bin/env bash

set -e

role=${CONTAINER_ROLE:-app}

if [ "$role" = "app" ]; then

    rm -f /etc/supervisor/conf.d/amqp.conf

    echo "Web mode is enabled"

elif [ "$role" = "worker" ]; then

    rm -f /etc/supervisor/conf.d/nginx.conf
    rm -f /etc/supervisor/conf.d/fpm.conf

    echo "Worker mode is enabled"
else
    echo "Could not match the container role \"$role\""
    exit 1
fi

/usr/bin/supervisord -c /etc/supervisor/conf.d/supervisord.conf